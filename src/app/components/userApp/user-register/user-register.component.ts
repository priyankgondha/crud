import { Component, OnInit } from '@angular/core';
import { userObj } from 'src/interface/user';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  userObj: userObj;
  isSaved: boolean = false;
  registrationform!: FormGroup;

  constructor() {
    this.userObj = new userObj();
    this.registrationform = new FormGroup({
      'userName': new FormControl(null, [Validators.required, Validators.minLength(3)]),
      'userCity': new FormControl(null, [Validators.required, Validators.minLength(3)]),
      'userState': new FormControl(null, [Validators.required]),
      'userMobile': new FormControl(null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
      'userAltMobile': new FormControl(null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")])
    });


  }
  get userName() {
    return this.registrationform.get('userName');
  }
  get userCity() {
    return this.registrationform.get('userCity');
  }
  get userState() {
    return this.registrationform.get('userState');
  }
  get userMobile() {
    return this.registrationform.get('userMobile');
  }
  get userAltMobile() {
    return this.registrationform.get('userAltMobile');
  }

  ngOnInit(): void {
  }
  getnewuserid() {
    const oldrecords = localStorage.getItem('userList');
    if (oldrecords !== null) {
      const userList = JSON.parse(oldrecords);
      return userList.length + 1;
    } else {
      return 1;
    }
  }

  saveUser() {
    const latestId = this.getnewuserid();
    const oldrecords = localStorage.getItem('userList');
    this.userObj.userId = latestId;
    if (oldrecords !== null) {
      const userList = JSON.parse(oldrecords);
      userList.push(this.userObj);
      localStorage.setItem('userList', JSON.stringify(userList));

    } else {
      const userarr = [];
      userarr.push(this.userObj as never);
      localStorage.setItem('userList', JSON.stringify(userarr));
    }
    this.isSaved = true;
  }

  newdata() {
    window.location.reload();
  }
  close() {
    this.isSaved = false;
  }

}
