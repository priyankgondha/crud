import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { userObj } from 'src/interface/user';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  userObj: userObj;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.userObj = new userObj();
    this.route.params.subscribe((res) => {
      this.userObj.userId = res['id']
    })
  }

  ngOnInit(): void {
    const oldrecords = localStorage.getItem('userList');
    if (oldrecords !== null) {
      const userList = JSON.parse(oldrecords);
      const currentUser = userList.find((m: any) => m.userId == this.userObj.userId)
      if (currentUser !== undefined) {
        this.userObj.userName = currentUser.userName;
        this.userObj.userCity = currentUser.userCity;
        this.userObj.userState = currentUser.userCity;
        this.userObj.userMobile = currentUser.userMobile;
        this.userObj.userAltMobile = currentUser.userAltMobile;


      }
    }
  }


  updateUser() {
    console.log("Hello");

    const oldrecords = localStorage.getItem('userList');
    if (oldrecords !== null) {
      const userList = JSON.parse(oldrecords);
      let index = userList.findIndex((m: any) => m.userId == this.userObj.userId)
      userList[index] = this.userObj
      localStorage.setItem('userList', JSON.stringify(userList));
      console.log("Ho");
    }
    this.router.navigateByUrl('/userlist')
  }

}
